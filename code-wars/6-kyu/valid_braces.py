# Write a function that takes a string of braces, and determines if the order of the braces is valid. It should return true if the string is valid, and false if it's invalid.

# This Kata is similar to the Valid Parentheses Kata, but introduces new characters: brackets [], and curly braces {}. Thanks to @arnedag for the idea!

# All input strings will be nonempty, and will only consist of parentheses, brackets and curly braces: ()[]{}.

# What is considered Valid?
# A string of braces is considered valid if all braces are matched with the correct brace.

# Examples
# "(){}[]"   =>  True
# "([{}])"   =>  True
# "(}"       =>  False
# "[(])"     =>  False
# "[({})](]" =>  False





#### POTENTIAL SOLUTIONS ####
# loop over string
#     at every open, begin nested loop

# Recursive??
# recursive_function (string, open_index)


# See opener, hold it
#   Create dictionary for (), [], and {} - and save indices to them
#   Create dictionary with openers for keys and closers for values
#       Maybe [brace_type, index] : index | null
#   Dictionaries might not work, since it'll be hard to check e.g. most recent opener
#
#   Create 3 lists of indices for (, {, and [
#
# See closer, match with opener
#   If no match, return False
#   To match:
#       Hold closer in variable
#       Search for most recent, same-brace, unmatched opener
#           From that opener

# For every (, look for the next ) and then check that it's within its nest?
# For every [ or { do the same

#### WILD IDEAS ####
# Mathematically determine true/false without matching indiviual braces
#   # of same-brace openers = # of same-brace closers within each nest
#       How to compartmentalize nests? Ideally, without matching?

# Iterate once to assign all opener & closer indices to lists; work strictly with lists
#   6 lists, 2x3 = open/close x brace type
#   Begin at the last opener, match with the first closer - no (([]()()))
#   Begin at the first closer, match with the most recent opener (([]*()*())); ([)]
#       If no match: return false
#       Check that even number of characters are in-between
#       Remove from string-copy & lists (([]()))
#           Repeat (([]))

def valid_braces(string):
  round_openers = []
  square_openers = []
  curly_openers = []
  round_closers = []
  square_closers = []
  curly_closers = []
  for index, char in enumerate(string):
    if char == "(":
      round_openers.append(index)
    elif char == "[":



#### LOGIC ####
# for every opening, a closing must follow.
# For every nested opening, the closing must be nested as well
# Each open begins a loop, each nested open begins a nested loop

# Return false if:
# Opener does not have a corresponding closer (inside its nest)
# Closer does not have a corresponding opener (inside its nest)]

# Return true if:
# Not false?
# Every open is closed (within its nest)

# To determine nesting:
# What was the last unclosed opener?
    Iterate backwards:


# To determine if an open is closed:
#

([}])

()({})
( [][] {()[]} )
